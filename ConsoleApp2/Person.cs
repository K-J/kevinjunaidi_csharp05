﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Person
    {
        public Person(string name, string address, DateTime dateofbirth, Enumethnic ethnic)
        {
            Name = name;
            Address = address;
            DateofBirth = dateofbirth;
            Ethnic = ethnic;
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public DateTime DateofBirth { get; set; }

        public int Age
        {
            get
            {
                var today = DateTime.Today;

                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (DateofBirth.Year * 100 + DateofBirth.Month) * 100 + DateofBirth.Day;

                return (a - b) / 10000;
            }
        }
        public Enumethnic Ethnic { get; set; }
    }

        public enum Enumethnic
        {
            Chinese = 0,
            Japanese = 1,
            Korean = 2,
            American = 3,
        }
    }
