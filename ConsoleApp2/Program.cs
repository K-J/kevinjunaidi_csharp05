﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> persons = new List<Person>();
            persons.Add(new Person("Kevin", "Tanjungpinang", new DateTime(2001, 09, 28), Enumethnic.Chinese));
            persons.Add(new Person("Junaidi", "Batam", new DateTime(2004, 07, 19), Enumethnic.Japanese));
            persons.Add(new Person("Agnes", "Batam", new DateTime(2001, 12, 24), Enumethnic.Chinese));
            persons.Add(new Person("Jocelyn", "Tanjungpinang", new DateTime(2002, 01, 17), Enumethnic.Japanese));
            persons.Add(new Person("Flavio", "Bali", new DateTime(2007, 11, 04), Enumethnic.American));
            persons.Add(new Person("Flavia", "Jakarta", new DateTime(2002, 09, 24), Enumethnic.American));
            persons.Add(new Person("Surtini", "Yogyakarta", new DateTime(1999, 08, 17), Enumethnic.Chinese));
            persons.Add(new Person("Shusanto", "Singapura", new DateTime(1997, 02, 16), Enumethnic.Korean));
            persons.Add(new Person("Evan", "Tanjungpinang", new DateTime(2003, 09, 14), Enumethnic.Korean));
            persons.Add(new Person("Welly", "Batam", new DateTime(1996, 07, 22), Enumethnic.Japanese));


            List<Person> Filter = new List<Person>();

            Filter = persons.Where(x => x.Ethnic == Enumethnic.Chinese && x.Age > 20).ToList();
        }


    }
}
